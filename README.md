# solid-character

Editor which allows adding [Cognitive Characteristics](http://smiy.sourceforge.net/cco/spec/cognitivecharacteristics.html).

## stage I

CLI which one can use in browser's console and use *Q* identifiers from https://www.wikidata.org

```js
cli.login('http://me.idp.dev/')
cli.list()
cli.add('Q2005')
cli.delete(cli.list()[0])
cli.setLanguage('es')
cli.list()
cli.search('aguacate')
```

## stage II

UI build with [Polymer 2.0](https://github.com/polymer/polymer/tree/2.0-preview) and proper [i18n](https://github.com/PolymerElements/app-localize-behavior)

### i18n

[<app-localize-behavior>](https://elements.polymer-project.org/elements/app-localize-behavior)
* minimal set of interface messages, translated on web translation platform
* [x] topic labels fetched from wikidata and cached, redux keeps labels object with all labels indexed by IRI of topic



## development

```shell
cp config.example.json config.json
npm install
npm run bundle
bower install
polymer serve
```

![diagram](doc/arch.png)


### TODO
* [ ] test !!!
* [x] create container in root, if one not registered
 * [ ] support solid registration as either 'container' or 'instance'
 * [ ] use ldp:IndirectContainer
* [x] bootstrap
 * [x] register *cco:CognitiveCharacteristics* in type index
 * [ ] allow choice of storage if multiple present
 * [x] set container public read
* [x] add topic
 * [x] include rdfs:label of the topic from wikidata (all languages)
  * [ ] use dedicated saga
* [x] fetch all
* [x] delete topic
* [x] search wikidata
* [ ] update cached wikidata labels

import $rdf from 'rdflib'
import * as ActionTypes from './actionTypes'
import { search, labelsFor } from './util'

export default class Cli {
  constructor (store) {
    this.$rdf = $rdf
    this.store = store
    this.getProfile = () => this.store.getState().profile
  }

  login (webid) {
    this.store.dispatch({
      type: ActionTypes.FETCH_PROFILE_REQUESTED,
      webid: webid
    })
  }

  add (wikidataId) {
    this.store.dispatch({
      type: ActionTypes.ADD_TOPIC_REQUESTED,
      profile: this.getProfile(),
      wikidataId
    })
  }

  delete (topic) {
    this.store.dispatch({
      type: ActionTypes.DELETE_TOPIC_REQUESTED,
      topic
    })
  }

  list () {
    return this.store.getState().topics.reduce((result, topic) => {
      let labels = labelsFor(topic)
      let labelForCurrentLanguage = labels.find(l => l.object.lang === this.store.getState().language).object.value
      result[labelForCurrentLanguage] = topic
      return result
    }, {})
  }

  setLanguage (language) {
    this.store.dispatch({
      type: ActionTypes.SET_LANGUAGE,
      language
    })
  }

  search (term) {
    return search(term, { language: this.store.getState().language })
      .then((results) => {
        results.forEach((result) => {
          console.log(result.id + ' | ' + result.label + ': ' + result.description)
        })
      })
  }
}

import fetch from 'isomorphic-fetch'
import solid from 'solid-client'
import $rdf from 'rdflib'
import { includePermalinks } from 'solid-permalinks'
import { includeCharacter } from './character'

const vocab = {
  cco: $rdf.Namespace('http://purl.org/ontology/cco/core#'),
  foaf: $rdf.Namespace('http://xmlns.com/foaf/0.1/'),
  rdfs: $rdf.Namespace('http://www.w3.org/2000/01/rdf-schema#')
}

// TODO: include in solid-permissions https://github.com/solid/solid-permissions/issues/10
function setPublic (containerUri, owner) {
  console.log(solid)
  return solid.web.head(containerUri)
    .then((response) => {
      return new solid.acl.PermissionSet(containerUri, response.aclAbsoluteUrl(), true, { rdf: solid.rdflib, webClient: solid.web })
        .addPermission(owner, solid.acl.Authorization.ALL_MODES)
        .addPermission(solid.acl.Authorization.acl.EVERYONE, solid.acl.Authorization.acl.READ)
        .save()
    }).then(response => Promise.resolve(containerUri))
}

function fetchProfile (webid) {
  // FIXME: https://github.com/solid/solid-client/issues/128
  return solid.getProfile(webid)
    .then(profile => profile.loadTypeRegistry())
    .then(profile => includePermalinks(profile))
    .then(profile => includeCharacter(profile))
}

function labelsFor (topic) {
  let entity = topic.value.statementsMatching(
    $rdf.sym(topic.uri),
    vocab.foaf('primaryTopic')
  )[0].object
  let node = topic.value.statementsMatching(
    entity,
    vocab.cco('topic')
  )[0].object
  return topic.value.statementsMatching(
    node,
    vocab.rdfs('label')
  )
}

const WIKIDATA_API = 'https://www.wikidata.org/w/api.php?action=wbsearchentities&origin=*&format=json&type=item'
const DEFAULT_OPTIONS = { limit: 10 }
/**
 * @param term - term to search
 * @param options - options for search and results
 */
function search (term, options) {
  let url = WIKIDATA_API + '&search=' + term
  options = Object.assign(DEFAULT_OPTIONS, options)
  if (options.language) {
    url += '&language=' + options.language + '&uselang=' + options.language
  }
  url += '&limit=' + options.limit
  return fetch(url)
    .then(response => response.json())
    .then(json => json.search)
}

export { search, setPublic, fetchProfile, labelsFor }

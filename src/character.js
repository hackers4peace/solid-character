import solid from 'solid-client'
import { createPermalink } from 'solid-permalinks'
import $rdf from 'rdflib'
import cuid from 'cuid'
const config = require('../config.json')

solid.vocab.cco = $rdf.Namespace('http://purl.org/ontology/cco/core#')
solid.vocab.wd = $rdf.Namespace('http://www.wikidata.org/entity/')
solid.vocab.verb = $rdf.Namespace('https://w3id.org/verb/')

import { setPublic } from './util'

function getMapping (url, profile) {
  if (profile.permalinkService) {
    return createPermalink(url, profile.permalinkService)
  } else {
    return Promise.resolve({
      entity: url + '#' + cuid(),
      doc: url
    })
  }
}

/**
 * TODO: check if already exists
 */
function addTopic (wikidataId, profile) {
  let topic = solid.vocab.wd(wikidataId)
  let resource = {
    value: $rdf.graph()
  }
  return solid.web.post(profile.character.getRegistration().locationUri, '', cuid())
    .then((response) => {
      resource.uri = response.url
      return getMapping(response.url, profile)
    }).then((mapping) => {
      let subject = $rdf.sym(mapping.entity)
      let graph = resource.value
      let relation = solid.vocab.cco('cognitive_characteristic')
      graph.add($rdf.sym(mapping.doc), solid.vocab.foaf('primaryTopic'), subject)
      graph.add(subject, solid.vocab.rdf('type'), solid.vocab.cco('CognitiveCharacteristic'))
      graph.add(subject, solid.vocab.cco('agent'), $rdf.sym(profile.webId))
      graph.add(subject, solid.vocab.cco('topic'), topic)
      graph.add(subject, solid.vocab.cco('characteristic'), relation)
      // binary relation for simpler querying
      graph.add($rdf.sym(profile.webId), relation, topic)
      // TODO: change to fetch to avoid attempts to get headers not exposed in CORS
      // TODO: handle with dedicated saga
      solid.web.config.proxyUrl = config.proxyUriTemplate
      return solid.web.get(topic.value, {forceProxy: true, noCredentials: true})
    }).then((response) => {
      let labels = response.parsedGraph().statementsMatching(topic, solid.vocab.rdfs('label'), null)
      resource.value.add(labels)
      return solid.web.put(resource.uri, resource.value.toNT().slice(1, -1))
    }).then((response) => { return resource })
}

/*
 * 1. create empty document
 * * create permalink (if service in preferences)
 * 2. create content for entity + document
 * * TODO: sign
 * 3. update document with content
 * 4. fetch labels from wikidata
 * 5. update documents with labels
 */
function addCharacteristic (profile, characteristics, topic, relation, activity) {
  getMapping(topic.uri, profile)
    .then((mapping) => {
      let subject = $rdf.sym(mapping.entity)
      let graph = topic.value
      graph.add($rdf.sym(mapping.doc), solid.vocab.foaf('primaryTopic'), subject)
      graph.add(subject, solid.vocab.rdf('type'), solid.vocab.cco('CognitiveCharacteristic'))
      graph.add(subject, solid.vocab.cco('agent'), $rdf.sym(profile.webId))
      graph.add(subject, solid.vocab.cco('topic'), topic)
      graph.add(subject, solid.vocab.cco('characteristic'), relation)
      if (activity) {
        graph.add(subject, solid.vocab.cco('activity'), activity)
      }
      // binary relation for simpler querying
      graph.add($rdf.sym(profile.webId), relation, topic)

      return solid.web.put(mapping.doc, graph.toNT().slice(1, -1))
    })
}

function includeCharacter (profile) {
  profile.character = {
    getRegistration () {
      // TODO: handle multiple registrations
      return profile.typeRegistryForClass(solid.vocab.cco('CognitiveCharacteristic'))[0]
    },
    bootstrap () {
      // TODO: add rdfs:label "Character"@en
      // TODO: check if profile.storage[0] exists and allow choosing storage if many
      return solid.web.createContainer(profile.storage[0], cuid())
        .then(response => setPublic(response.url, profile.webId))
        .then(containerUri => profile.registerType(solid.vocab.cco('CognitiveCharacteristic'), containerUri, 'container', true))
    }
  }
  return profile
}

export { addTopic, includeCharacter }

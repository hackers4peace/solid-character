import 'babel-polyfill'
import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import createLogger from 'redux-logger'

import Cli from './cli'
import * as ActionTypes from './actionTypes'
import reducer from './reducers'
import sagas from './sagas'

const sagaMiddleware = createSagaMiddleware()
const logger = createLogger()
const store = createStore(
  reducer,
  applyMiddleware(sagaMiddleware, logger)
)

sagaMiddleware.run(sagas)

function observe () {
  let state = store.getState()
  if (state.profile &&
      state.profile.character.getRegistration() &&
      !state.loaded.topics &&
      !state.requested.topics) {
    store.dispatch({
      type: ActionTypes.FETCH_TOPICS_REQUESTED,
      profile: state.profile
    })
  }
  if (state.profile &&
      !state.profile.character.getRegistration() &&
      !state.requested.bootstrap) {
    store.dispatch({
      type: ActionTypes.BOOTSTRAP_REQUESTED,
      profile: state.profile
    })
  }
}
store.subscribe(observe)

window.cli = new Cli(store)

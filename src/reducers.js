import { combineReducers } from 'redux'
import * as ActionTypes from './actionTypes'
import { labelsFor } from './util.js'
const config = require('../config.json')

function profile (state = null, action) {
  switch (action.type) {
    case ActionTypes.FETCH_PROFILE_SUCCEEDED:
      return action.profile
    case ActionTypes.BOOTSTRAP_SUCCEEDED:
      return action.profile
    default:
      return state
  }
}

function topics (state = [], action) {
  switch (action.type) {
    case ActionTypes.FETCH_TOPICS_SUCCEEDED:
      return action.contents
    case ActionTypes.ADD_TOPIC_SUCCEEDED:
      return [
        ...state,
        action.resource
      ]
    case ActionTypes.DELETE_TOPIC_SUCCEEDED:
      let index = state.indexOf(action.topic)
      return [
        ...state.slice(0, index),
        ...state.slice(index + 1)
      ]
    case ActionTypes.FETCH_PROFILE_FAILED:
    case ActionTypes.FETCH_CONTENTS_FAILED:
    case ActionTypes.ADD_TOPIC_FAILED:
      // TODO: handle failed
      return state
    default:
      return state
  }
}

function searchResults (state = [], action) {
  switch (action.type) {
    case ActionTypes.SEARCH_SUCCEEDED:
      return action.results
    case ActionTypes.SEARCH_FAILED:
      // TODO: handle failed
      return state
    default:
      return state
  }
}

function language (state = config.language, action) {
  switch (action.type) {
    case ActionTypes.SET_LANGUAGE:
      return action.language
    default:
      return state
  }
}

function labels (state = {}, action) {
  switch (action.type) {
    case ActionTypes.FETCH_TOPICS_SUCCEEDED:
      return action.contents.reduce((result, topic) => {
        labelsFor(topic).forEach((label) => {
          if (!result[label.object.lang]) result[label.object.lang] = {}
          result[label.object.lang][topic.uri] = label.object.value
        })
        return result
      }, {})
    case ActionTypes.ADD_TOPIC_SUCCEEDED:
      let merged = Object.create(state)
      labelsFor(action.resource).forEach((label) => {
        if (!state[label.object.lang]) merged[label.object.lang] = {}
        merged[label.object.lang][action.resource.uri] = label.object.value
      })
      return merged
    default:
      return state
  }
}

function requested (state = {}, action) {
  switch (action.type) {
    case ActionTypes.BOOTSTRAP_REQUESTED:
      return {
        ...state,
        bootstrap: true
      }
    case ActionTypes.BOOTSTRAP_SUCCEEDED:
      return {
        ...state,
        bootstrap: false
      }
    case ActionTypes.FETCH_TOPICS_REQUESTED:
      return {
        ...state,
        topics: true
      }
    case ActionTypes.FETCH_TOPICS_SUCCEEDED:
      return {
        ...state,
        topics: false
      }
    default:
      return state
  }
}

// TODO: refactor to remove this part of the state
function loaded (state = {}, action) {
  switch (action.type) {
    case ActionTypes.FETCH_TOPICS_SUCCEEDED:
      return {
        ...state,
        topics: true
      }
    default:
      return state
  }
}

export default combineReducers({
  profile,
  topics,
  searchResults,
  language,
  labels,
  requested,
  loaded
})

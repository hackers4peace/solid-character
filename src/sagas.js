import { takeLatest, takeEvery } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import solid from 'solid-client'
import * as ActionTypes from './actionTypes'
import { addTopic } from './character'
import { fetchProfile, search as searchWikidata } from './util'

function * login (action) {
  try {
    const profile = yield call(fetchProfile, action.webid)
    yield put({
      type: ActionTypes.FETCH_PROFILE_SUCCEEDED,
      profile
    })
  } catch (e) {
    yield put({
      type: ActionTypes.FETCH_PROFILE_FAILED,
      message: e.message
    })
  }
}

function * bootstrap (action) {
  try {
    const profile = yield call(action.profile.character.bootstrap)
    yield put({
      type: ActionTypes.BOOTSTRAP_SUCCEEDED,
      profile
    })
  } catch (e) {
    yield put({
      type: ActionTypes.BOOTSTRAP_FAILED,
      message: e.message
    })
  }
}

function * fetch (action) {
  try {
    // TODO: use call or apply effect
    let containerUri = action.profile.character.getRegistration().locationUri
    const contents = yield solid.web.get(containerUri)
      .then(container => solid.web.loadParsedGraphs(container.contentsUris))
    yield put({
      type: ActionTypes.FETCH_TOPICS_SUCCEEDED,
      contents
    })
  } catch (e) {
    yield put({
      type: ActionTypes.FETCH_TOPICS_FAILED,
      message: e.message
    })
  }
}

function * add (action) {
  try {
    // TODO: use call or apply effect
    const resource = yield addTopic(
      action.wikidataId,
      action.profile
    )
    yield put({
      type: ActionTypes.ADD_TOPIC_SUCCEEDED,
      resource
    })
  } catch (e) {
    yield put({
      type: ActionTypes.ADD_TOPIC_FAILED,
      message: e.message
    })
  }
}

function * del (action) {
  try {
    yield solid.web.del(action.topic.uri)
    yield put({
      type: ActionTypes.DELETE_TOPIC_SUCCEEDED,
      topic: action.topic
    })
  } catch (e) {
    yield put({
      type: ActionTypes.DELETE_TOPIC_FAILED,
      message: e.message
    })
  }
}

function * search (action) {
  try {
    const results = yield searchWikidata(action.term, { language: action.language })
    yield put({
      type: ActionTypes.SEARCH_SUCCEEDED,
      results
    })
  } catch (e) {
    yield put({
      type: ActionTypes.SEARCH_FAILED,
      message: e.message
    })
  }
}

function * loginSaga () {
  yield * takeLatest(ActionTypes.FETCH_PROFILE_REQUESTED, login)
}

function * bootstrapSaga () {
  yield * takeLatest(ActionTypes.BOOTSTRAP_REQUESTED, bootstrap)
}

function * fetchSaga () {
  yield * takeLatest(ActionTypes.FETCH_TOPICS_REQUESTED, fetch)
}

function * addSaga () {
  yield * takeEvery(ActionTypes.ADD_TOPIC_REQUESTED, add)
}

function * delSaga () {
  yield * takeEvery(ActionTypes.DELETE_TOPIC_REQUESTED, del)
}

function * searchSaga () {
  yield * takeLatest(ActionTypes.SEARCH_REQUESTED, search)
}

function * root () {
  yield [
    call(loginSaga),
    call(bootstrapSaga),
    call(fetchSaga),
    call(addSaga),
    call(delSaga),
    call(searchSaga)
  ]
}

export default root
